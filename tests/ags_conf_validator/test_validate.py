import unittest
from ags_conf_validator.validator import validate
from nose.tools import eq_
from yamale.yamale_error import YamaleError


class TestExtracConfig(unittest.TestCase):
    def test_returnNone_when_passConfig(self):
        config_path = './tests/resources/service.yml'
        result = validate(config_path)
        eq_(result, None)

    def test_returnException_when_passErrorConfig(self):
        config_path = './tests/resources/service-error.yml'
        with self.assertRaises(YamaleError):
            validate(config_path)
