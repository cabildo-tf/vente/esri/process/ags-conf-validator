import setuptools

setuptools.setup(
    name="ags-conf-validator",
    version="0.0.1",
    author="Ignacio Lorenzo",
    author_email="nacholore@gmail.com",
    description="Validator of arcgis service configuration",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
          'yamale'
      ],
    entry_points={
        'console_scripts': ['ags-conf-validator=ags_conf_validator.validator:main'],
    }
)
