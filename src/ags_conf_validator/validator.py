import os
import yaml
import yamale
import argparse
import logging


def load_config(path):
    if path and os.path.exists(path):
        with open(path, 'r') as f:
            return yaml.safe_load(f.read())


def validate(config):
    config = yamale.make_data(config)
    path = os.path.join(os.path.dirname(__file__), 'schema.yaml')
    schema = yamale.make_schema(path)
    yamale.validate(schema, config)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--service-config", dest="service_config_path", default="service.yml")
    args = parser.parse_args()
    logging.info("Starting")
    logging.info("Loading configuration")
    validate(config=args.service_config_path)
    logging.info("Configuration loaded")


if __name__ == '__main__':
    main()
